var gulp = require('gulp');
var concat = require('gulp-concat');
var path = require('path');
var sourcemaps = require('gulp-sourcemaps');
var less = require('gulp-less');


var Bower =  './bower_components';
var Less  = './app/Resources/public/less';
var publicDir = './web/Resources/public';
var LessToCss = './web/Resources/public/css';
var npmDir = './node_modules';

var style = {
    cssDir:publicDir + '/css',
    jsDir: publicDir + '/js',
    fontDir: publicDir + '/fonts'
};



var config = {
    globalLess: [
        Less + '/style.less'

    ],
    layoutJS:[
        Bower + '/jquery/dist/jquery.js'
    ]
};

var bundleJS = {
    interfaceBundleJS: [
        Bower + '/jquery/dist/jquery-ui.js'
    ],
    userBundleJS: [
        Bower + '/jquery/dist/example.js'
    ]
};


/*********************************************************
 * CSS
 */

gulp.task('less', function () {
    return gulp.src(config.globalLess)
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(concat('main.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(style.cssDir));
});

/*********************************************************
 * JS
 */

gulp.task('layout-js', function () {
    gulp.src(config.layoutJS)
        .pipe(sourcemaps.init())
        .pipe(concat('layout.js'))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(style.jsDir))
});