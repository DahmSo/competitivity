# **STRIVE**

Texte d'explication

## == Requirements ==

Bower

```
$ npm install -g bower
```

Gulp

```
$ npm install gulp-cli -g
```

## == Symfony ==

Version 3.0

## == Installation ==

Clone the Project

```
git clone git@gitlab.com:DahmSo/competitivity.git
```

Install dependencies

```
composser install
```


## == Scripts Included ==

> ## Credits
> - http://materializecss.com
> - https://jquery.com
> - http://jqueryui.com


Project is developed and maintained by Dahmani Sofiane @SimplonLyon 


