<?php

namespace ApplicationBundle\Controller;

use ApplicationBundle\Entity\Event;
use ApplicationBundle\Form\EventType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ApplicationController extends Controller
{

    /**
     * @Route("/create_event", name="createEvent")
     * @Security("has_role('ROLE_USER') or has_role('ROLE_ADMIN')")
     */
    public function createEventAction(Request $request)
    {
        $user = $this->getUser();

        $event = new Event();
        $form = $this->createForm(EventType::class, $event)->add('submit', SubmitType::class, [
            'label' => 'Create',
            'attr' => ['class' => 'button'],
        ]);

        $event->setUser($user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $requestParams = $request->request->all();
            $event->setEventStartDate(new \DateTime($requestParams['applicationbundle_event']['eventStartDate']));
            $event->setEventEndDate(new \DateTime($requestParams['applicationbundle_event']['eventEndDate']));
            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();


            return $this->redirectToRoute('displayMyEvent');
        }

        $template = $this->render('@Application/createEvent.html.twig', array(
            'form' => $form->createView()
        ));


        return $template;

    }

    /**
     * @Route("/display_all_event", name="displayAllEvent")
     */
    public function displayAllEventAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ApplicationBundle:Event');
        $events = $repo->findAll();

        $template = $this->render('@Application/displayAllEvent.html.twig', array('events' => $events));

        return $template;

    }

    /**
     * @Route("/get_all_event_data", options = { "expose" = true }, name="getAllEventData")
     */
    public function getAllEventDataAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ApplicationBundle:Event');
        $userId = $this->getUser()->getId();

        $events = $repo->findAllEventBesidesMine($userId);

        return new JsonResponse(array('events' => $events));

    }

    /**
     * @Route("/event_detail/{eventId}", options = { "expose" = true }, name="eventDetail")
     */
    public function eventDetailAction(Request $request, $eventId)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ApplicationBundle:Event');
        $event = $repo->find($eventId);
        $participants = $event->getParticipants();
        $allowinscription = true;

        $eventStartDate = $event->getEventStartDate();
        $eventEndDate = $event->getEventEndDate();
        $eventName = $event->getEventName();
        $eventDescription = $event->getEventDescription();
        $eventLocation = $event->getEventLocation();
        $eventCoords = [$event->getEventLatitude(), $event->getEventLongitude()];


        $eventCreator = $event->getUser();
        $eventCreatorName = $eventCreator->getUsername();

        $participantsLength = count($participants);
        $participantsLimit = $event->getEventParticipantLimit();

        if($participantsLength > $participantsLimit)
        {
            $allowinscription = false;
        }



        $response = new JsonResponse(array(
            'eventStartDate' => $eventStartDate,
            'eventEndDate' => $eventEndDate,
            'eventName' => $eventName,
            'eventDescription' => $eventDescription,
            'eventLocation' => $eventLocation,
            'eventCreatorName' => $eventCreatorName,
            'allowInscription' => $allowinscription,
            'eventCoords' => $eventCoords));

        return $response;
    }

    /**
     * @Route("delete_event/{eventId}", name="deleteEvent")
     * @Security("has_role('ROLE_USER') or has_role('ROLE_ADMIN')")
     */
    public function deleteEventAction(Request $request,$eventId)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ApplicationBundle:Event');
        $user = $this->getUser();

        $event = $repo->find($eventId);

        if(!$event){
            throw $this->createNotFoundException('Unable to find event');
        }

        $eventCreator = $event->getUser();

        if( $user === $eventCreator){

            $em->remove($event);
            $em->flush();

            return $this->redirectToRoute('displayMyEvent');
        } else {
            throw $this->createAccessDeniedException("Access denied");
        }

    }

    /**
     * @Route("participate_event/{eventId}", options = { "expose" = true }, name="participateEvent")
     * @Security("has_role('ROLE_USER') or has_role('ROLE_ADMIN')")
     */
    public function participateEventAction(Request $request , $eventId)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ApplicationBundle:Event');
        $user = $this->getUser();

        $event = $repo->find($eventId);
        $participants = $event->getParticipants();

        for ($i = 0; $i < count($participants); $i++) {
            if($participants->getValues()[$i]->getId() == $user->getId()){
                return new JsonResponse(array('data' => '1'));
            }
        }
        $participantsLength = count($participants);
        $participantsLimit = $event->getEventParticipantLimit();


        if($participantsLength > $participantsLimit)
        {
            return new JsonResponse(array('data' => '4'));
        }

        $event->addParticipant($user);


        $em->persist($event);
        $em->flush();

        return new JsonResponse(array('data' => '3'));


    }

    /**
     * @Route("remove_participation/{eventId}", name="removeParticipation")
     * @Security("has_role('ROLE_USER') or has_role('ROLE_ADMIN')")
     */
    public function removeParticipationEventAction(Request $request,$eventId)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('ApplicationBundle:Event');
        $user = $this->getUser();

        $event = $repo->find($eventId);

        if(!$event){
            throw $this->createNotFoundException('Unable to find event');
        }

        $event->removeParticipant($user);

        $em->persist($event);
        $em->flush();

        return $this->redirectToRoute('displayMyEvent');
    }

    /**
     * @Route("display_my_event", name="displayMyEvent")
     * @Security("has_role('ROLE_USER') or has_role('ROLE_ADMIN')")
     */
    public function displayMyEventAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('ApplicationBundle:Event');
        $user = $this->getUser();

        $userCreatedEvents = $events->findBy( ['user' => $user->getId()] );
        $userParticipatedEvents = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findUserParticipatedEvent($user->getId());

        $showUserParticipatedEvents = [];

        for ($i = 0; $i < count($userParticipatedEvents); $i++) {
            $showUserParticipatedEvent = $events->findBy( ['id' => $userParticipatedEvents[$i]['event_id']] );
            array_push($showUserParticipatedEvents,$showUserParticipatedEvent);
        }

        $template = $this->render('@Application/displayMyEvent.html.twig', array(
            'userCreatedEvents' => $userCreatedEvents,
            'showUserParticipatedEvents' => $showUserParticipatedEvents));

        return $template;
    }




}
