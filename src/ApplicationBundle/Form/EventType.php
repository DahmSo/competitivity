<?php

namespace ApplicationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('eventName',TextType::class, array('required'  => true))
            ->add('eventType', ChoiceType::class, array(
                'choices'   => array('Duo' => 'two', 'Collectif' => 'many'),
                'required'  => false,
                'attr' => array( 'class' => 'form-control')
            ))
            ->add('eventStartDate', TextType::class)
            ->add('eventEndDate', TextType::class)
            ->add('eventParticipantLimit', ChoiceType::class, array(
                'choices' =>range(2,20), 'attr' => array( 'class' => 'form-control')))
            ->add('eventLocation', TextType::class, array('attr' => array('readonly' => 'true')))
            ->add('eventDescription', TextareaType::class)
            ->add('eventLatitude',  HiddenType::class)
            ->add('eventLongitude',  HiddenType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ApplicationBundle\Entity\Event'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'applicationbundle_event';
    }


}
