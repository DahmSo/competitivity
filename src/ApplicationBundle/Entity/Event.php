<?php

namespace ApplicationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="ApplicationBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="event_name", type="string", length=255)
     */
    private $eventName;

    /**
     * @var string
     *
     * @ORM\Column(name="event_type", type="string", length=255)
     */
    private $eventType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_start_date", type="datetime")
     */
    private $eventStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_end_date", type="datetime")
     */
    private $eventEndDate;

    /**
     * @var string
     *
     * @ORM\Column(name="event_participant_limit", type="string", length=255)
     */
    private $eventParticipantLimit;

    /**
     * @var string
     *
     * @ORM\Column(name="event_location", type="string", length=255)
     */
    private $eventLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="event_latitude",  type="string", length=255)
     */
    private $eventLatitude;

    /**
     * @var string
     *
     * @ORM\Column(name="event_longitude",  type="string", length=255)
     */
    private $eventLongitude;

    /**
     * @var string
     *
     * @ORM\Column(name="event_description", type="string", length=255)
     */
    private $eventDescription;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="events")
     */
    private $user;

    /**
     * @var Participants
     *
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User", inversedBy="events")
     */
    private $participants;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set eventName
     *
     * @param string $eventName
     *
     * @return Event
     */
    public function setEventName($eventName)
    {
        $this->eventName = $eventName;

        return $this;
    }

    /**
     * Get eventName
     *
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param string $eventType
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * Set eventStartDate
     *
     * @param \DateTime $eventStartDate
     *
     * @return Event
     */
    public function setEventStartDate($eventStartDate)
    {
        $this->eventStartDate = $eventStartDate;

        return $this;
    }

    /**
     * Get eventStartDate
     *
     * @return \DateTime
     */
    public function getEventStartDate()
    {
        return $this->eventStartDate;
    }

    /**
     * Set eventEndDate
     *
     * @param \DateTime $eventEndDate
     *
     * @return Event
     */
    public function setEventEndDate($eventEndDate)
    {
        $this->eventEndDate = $eventEndDate;

        return $this;
    }

    /**
     * Get eventEndDate
     *
     * @return \DateTime
     */
    public function getEventEndDate()
    {
        return $this->eventEndDate;
    }

    /**
     * Set eventParticipantLimit
     *
     * @param string $eventParticipantLimit
     *
     * @return Event
     */
    public function setEventParticipantLimit($eventParticipantLimit)
    {
        $this->eventParticipantLimit = $eventParticipantLimit;

        return $this;
    }

    /**
     * Get eventParticipantLimit
     *
     * @return string
     */
    public function getEventParticipantLimit()
    {
        return $this->eventParticipantLimit;
    }

    /**
     * Set eventLocation
     *
     * @param string $eventLocation
     *
     * @return Event
     */
    public function setEventLocation($eventLocation)
    {
        $this->eventLocation = $eventLocation;

        return $this;
    }

    /**
     * Get eventLocation
     *
     * @return string
     */
    public function getEventLocation()
    {
        return $this->eventLocation;
    }

    /**
     * @return int
     */
    public function getEventLatitude()
    {
        return $this->eventLatitude;
    }

    /**
     * @param int $eventLatitude
     */
    public function setEventLatitude($eventLatitude)
    {
        $this->eventLatitude = $eventLatitude;
    }

    /**
     * @return int
     */
    public function getEventLongitude()
    {
        return $this->eventLongitude;
    }

    /**
     * @param int $eventLongitude
     */
    public function setEventLongitude($eventLongitude)
    {
        $this->eventLongitude = $eventLongitude;
    }

    /**
     * Set eventDescription
     *
     * @param string $eventDescription
     *
     * @return Event
     */
    public function setEventDescription($eventDescription)
    {
        $this->eventDescription = $eventDescription;

        return $this;
    }

    /**
     * Get eventDescription
     *
     * @return string
     */
    public function getEventDescription()
    {
        return $this->eventDescription;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Event
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add participant
     *
     * @param \UserBundle\Entity\User $participant
     *
     * @return Event
     */
    public function addParticipant(\UserBundle\Entity\User $participant)
    {
        $this->participants[] = $participant;

        return $this;
    }

    /**
     * Remove participant
     *
     * @param \UserBundle\Entity\User $participant
     */
    public function removeParticipant(\UserBundle\Entity\User $participant)
    {
        $this->participants->removeElement($participant);
    }

    /**
     * Get participants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipants()
    {
        return $this->participants;
    }
}
