<?php

namespace InterfaceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class InterfaceController extends Controller
{
    /**
     * @Route("/",  name="homepage")
     */
    public function indexAction()
    {
        if ($this->isGranted('ROLE_USER') == true) {
            return $this->render('InterfaceBundle::user_homepage.html.twig');
        } else {
            return $this->render('InterfaceBundle:Default:index.html.twig');
        }

    }

}
