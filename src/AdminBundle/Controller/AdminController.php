<?php

namespace AdminBundle\Controller;

use ApplicationBundle\Entity\Event;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin-index")
     */
    public function getAllUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('UserBundle:User');
        $users = $repo->findAll();
        Return $this->render('@Admin/index_admin.html.twig',array(
            'users'=>$users));
    }

    /**
     * @Route("/administrator", name="administrator")
     */
    public function getAllAdministratorAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('UserBundle:User');
        $admins = $repo->findByRole('ROLE_ADMIN');
        Return $this->render('@Admin/user_display_admin.html.twig',array('users' => $admins));
    }

    /**
     * @Route("/user", name="user")
     */
    public function getAllUserAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('UserBundle:User');
        $users = $repo->findByRole('ROLE_USER');
        Return $this->render('@Admin/user_display_admin.html.twig',array('users' => $users));
    }

    /**
     * @Route("/banned/{userId}", name="ban-user")
     */
    public function banThisUserAction(Request $request)
    {

    }

    /**
     * @Route("/user-events/{userId}", name="user-event")
     */
    public function getThisUserEvents(Request $request, $userId)
    {
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('ApplicationBundle:Event');

        $userCreatedEvents = $events->findBy( ['user' => $userId] );
        $userParticipatedEvents = $this->getDoctrine()
            ->getRepository(Event::class)
            ->findUserParticipatedEvent($userId);

        $showUserParticipatedEvents = [];

        for ($i = 0; $i < count($userParticipatedEvents); $i++) {
            $showUserParticipatedEvent = $events->findBy( ['id' => $userParticipatedEvents[$i]['event_id']] );
            array_push($showUserParticipatedEvents,$showUserParticipatedEvent);
        }

        $template = $this->render('@Application/displayMyEvent.html.twig', array(
            'userCreatedEvents' => $userCreatedEvents,
            'showUserParticipatedEvents' => $showUserParticipatedEvents));

        return $template;

    }

}
